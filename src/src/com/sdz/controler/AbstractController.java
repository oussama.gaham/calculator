package com.sdz.controler;

import com.sdz.model.AbstractModel;

import java.util.ArrayList;

public abstract class AbstractController {
  protected final AbstractModel calc;
  protected String operator = "";
  protected String number = "";
  protected final ArrayList<String> listOperator = new ArrayList<>();

  public AbstractController(AbstractModel cal) {
    this.calc = cal;

    this.listOperator.add("+");
    this.listOperator.add("-");
    this.listOperator.add("*");
    this.listOperator.add("/");
    this.listOperator.add("=");
  }

  public void setOperator(String ope) {
    this.operator = ope;
    control();
  }

  public void setNumber(String number) {
    this.number = number;
    control();
  }

  public void reset() {
    this.calc.reset();
  }

  abstract void control();
}
