package com.sdz.controler;

import com.sdz.model.AbstractModel;

public class CalculatorController extends AbstractController {
  public CalculatorController(AbstractModel cal) {
    super(cal);
  }

  @Override
  void control() {
    if (this.listOperator.contains(this.operator)) {
      if (this.operator.equals("="))
        this.calc.getResult();
      else
        this.calc.setOperator(this.operator);
    }

    if (this.number.matches("^[0-9.]+$"))
      this.calc.setNumber(this.number);

    this.operator = "";
    this.number = "";
  }
}
