package com.sdz.model;

import com.sdz.observer.Observable;
import com.sdz.observer.Observer;

import java.util.ArrayList;

public abstract class AbstractModel implements Observable {
  protected double result = 0;
  protected String operator = "";
  protected String operand = "";
  private final ArrayList<Observer> listObserver = new ArrayList<>();

  public abstract void reset();

  public abstract void getResult();

  public abstract void setOperator(String operator);

  public abstract void setNumber(String number);

  public void addObserver(Observer obs) {
    this.listObserver.add(obs);
  }

  public void notifyObserver(String str) {
    if (str.matches("^@[0-9]+"))
      str = str.substring(1);

    for (Observer obs : this.listObserver)
      obs.update(str);
  }

}