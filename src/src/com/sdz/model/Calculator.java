package com.sdz.model;

public class Calculator extends AbstractModel {

  @Override
  public void reset() {
    this.result = 0;
    this.operand = "0";
    this.operator = "";

    notifyObserver(String.valueOf(this.result));
  }

  public void calculate() {

    if (this.operator.equals(""))
      this.result = Double.parseDouble(this.operand);
    else {
      if (!this.operand.equals(""))
        switch (this.operator) {
          case "+":
            this.result += Double.parseDouble(this.operand);
            break;
          case "-":
            this.result -= Double.parseDouble(this.operand);
            break;
          case "*":
            this.result *= Double.parseDouble(this.operand);
            break;
          case "/":
            try {
              this.result /= Double.parseDouble(this.operand);
            } catch (ArithmeticException e) {
              this.result = 0;
            }
            break;
        }
    }
    this.operand = "";

    notifyObserver(String.valueOf(this.result));
  }

  @Override
  public void getResult() {
    calculate();
  }

  @Override
  public void setOperator(String operator) {
    calculate();

    this.operator = operator;

    if (!operator.equals("=")) {
      this.operand = "";
    }
  }

  @Override
  public void setNumber(String number) {
    this.operand += number;

    notifyObserver(this.operand);
  }
}
