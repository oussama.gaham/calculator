package com.sdz.main;

import com.sdz.controler.AbstractController;
import com.sdz.controler.CalculatorController;
import com.sdz.model.AbstractModel;
import com.sdz.vue.Calculator;

public class Main {

  public static void main(String[] args) {
    AbstractModel calc = new com.sdz.model.Calculator();

    AbstractController controller = new CalculatorController(calc);

    Calculator calculator = new Calculator(controller);

    calc.addObserver(calculator);
  }
}
