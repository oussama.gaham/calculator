package com.sdz.vue;

import com.sdz.controler.AbstractController;
import com.sdz.observer.Observer;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Calculator extends JFrame implements Observer {

  final String[] tab_string = {"1", "2", "3", "4", "5", "6", "7", "8", "9", "0", ".", "=", "C", "+", "-", "*", "/"};
  final JButton[] tab_button = new JButton[tab_string.length];
  private final JPanel container = new JPanel();
  private JLabel screen = new JLabel();
  private final Dimension dim = new Dimension(50, 40);
  private final Dimension dim2 = new Dimension(50, 31);

  private final AbstractController controller;

  public Calculator(AbstractController controller) {
    this.setSize(240, 260);
    this.setTitle("Calculator");
    this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    this.setLocationRelativeTo(null);
    this.setResizable(false);
    initComponent();
    this.controller = controller;
    this.setContentPane(container);
    this.setVisible(true);
  }

  private void initComponent() {
    Font police = new Font("Arial", Font.BOLD, 20);
    screen = new JLabel("0");
    screen.setFont(police);
    screen.setHorizontalAlignment(JLabel.RIGHT);
    screen.setPreferredSize(new Dimension(220, 20));

    JPanel operator = new JPanel();
    operator.setPreferredSize(new Dimension(55, 255));
    JPanel digit = new JPanel();
    digit.setPreferredSize(new Dimension(165, 225));
    JPanel screenPanel = new JPanel();
    screenPanel.setPreferredSize(new Dimension(220, 30));

    OperatorListener opeListener = new OperatorListener();

    for (int i = 0; i < tab_string.length; i++) {
      tab_button[i] = new JButton(tab_string[i]);
      tab_button[i].setPreferredSize(dim);

      switch (i) {
        case 11:
          tab_button[i].addActionListener(opeListener);
          digit.add(tab_button[i]);
          break;
        case 12:
          tab_button[i].setForeground(Color.red);
          tab_button[i].addActionListener(new ResetListener());
          tab_button[i].setPreferredSize(dim2);
          operator.add(tab_button[i]);
          break;
        case 13:
        case 14:
        case 15:
        case 16:
          tab_button[i].setForeground(Color.red);
          tab_button[i].addActionListener(opeListener);
          tab_button[i].setPreferredSize(dim2);
          operator.add(tab_button[i]);
          break;
        default:
          digit.add(tab_button[i]);
          tab_button[i].addActionListener((new DigitListener()));
          break;
      }
    }

    screenPanel.add(screen);
    screenPanel.setBorder(BorderFactory.createLineBorder(Color.BLACK));
    container.add(screenPanel, BorderLayout.NORTH);
    container.add(digit, BorderLayout.CENTER);
    container.add(operator, BorderLayout.EAST);
  }

  class DigitListener implements ActionListener {
    @Override
    public void actionPerformed(ActionEvent actionEvent) {
      controller.setNumber(((JButton)actionEvent.getSource()).getText());
    }
  }

  class OperatorListener implements  ActionListener {
    @Override
    public void actionPerformed(ActionEvent actionEvent) {
      controller.setOperator(((JButton)actionEvent.getSource()).getText());
    }
  }

  class ResetListener implements ActionListener {
    @Override
    public void actionPerformed(ActionEvent actionEvent) {
      controller.reset();
    }
  }

  public void update(String s) {
    screen.setText(s);
  }
}
