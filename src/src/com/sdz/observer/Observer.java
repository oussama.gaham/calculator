package com.sdz.observer;

public interface Observer {
  void update(String str);
}